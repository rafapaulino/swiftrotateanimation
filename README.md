# SwiftRotateAnimation

## Resources
This repository contains an example XCode project for the blog post at [andrewcbancroft.com](http://www.andrewcbancroft.com) titled [Rotate Animation in Swift](http://www.andrewcbancroft.com/2014/10/15/rotate-animation-in-swift/).

## Compatibility
Verified that this repository's code works in XCode 7.x with Swift 2.0
